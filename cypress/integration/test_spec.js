
const emailSelector = '[data-cy=email] input';
const passwordSelector = '[data-cy=password]'

describe("My First Test", function() {
    it("Finds and element", function() {
           cy.visit('http://localhost:3000/login')

           cy.contains('Forgot password?').click()

           cy.url()
            .should("include", "/reset")
    })

    it("Logging in", () => {
         // Arrange - setup initial app state
        /*
            - visit a page
            - query for an element
            - take an action
            - interact with that element
        */
        cy.visit("http://localhost:3000/login")
        cy.get(emailSelector)
            .type("admin1")
        cy.get(passwordSelector)
            .type("salasana")
        cy.get("form").submit()
        
        // Assert = make an assertion = Tee väite 
        //      - make an assertion about the page content e.g is the page showing correct information in it's current state?
        cy.url().should("include","/")
        
    })
})
import React, { useEffect, useState } from "react";
import "./App.css";

const axios = require("axios");

export default function App() {
  const [pic, setPic] = useState<string | undefined>();

  useEffect(() => {
    axios.get("https://api.thecatapi.com/v1/images/search").then((res: any) => {
      console.log(res.data[0]);
      setPic(res.data[0].url);
    });
  }, []);

  return (
    <>
      {pic !== undefined && (
        <img alt="cat" src={pic} width={400} height={400} />
      )}
    </>
  );
}

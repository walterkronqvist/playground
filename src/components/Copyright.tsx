import { Typography, Link } from "@mui/material";

export default function Copyright({ version }: any) {
  return (
    <Typography variant="body2">
      {version}
      {" Copyright © "}
      <Link href="https://livion.fi/">Livion Oy.</Link>{" "}
      {new Date().getFullYear()}
    </Typography>
  );
}

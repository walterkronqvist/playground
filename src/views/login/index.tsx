import * as React from "react";
import { useNavigate } from "react-router-dom";
import {
  Container,
  Box,
  TextField,
  Button,
  Divider,
  Avatar,
  Typography,
} from "@mui/material";

import LockOutlinedIcon from "@mui/icons-material/LockOutlined";
import { Link as RouterLink } from "react-router-dom";
import Copyright from "../../components/Copyright";

import { useForm, SubmitHandler } from "react-hook-form";

type Inputs = {
  email: string;
  password: string;
};

const Login = () => {
  // destructuring from the useForm hook that has a generic type <Inputs>?
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<Inputs>();
  const onSubmit: SubmitHandler<Inputs> = async (data) => {
    await sleep(2000);
    if (data.email === "admin1" && data.password === "salasana") {
      console.log("logging in");
      navigate("/");
    }
  };

  const navigate = useNavigate();
  const sleep = (ms: number) =>
    new Promise((resolve) => setTimeout(resolve, ms));

  return (
    <React.Fragment>
      <Container maxWidth="xs">
        <Box
          sx={{
            marginTop: 8,
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
          }}
        >
          <Avatar sx={{ m: 1, bgcolor: "primary.light" }}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography variant="h5">Sign in</Typography>
        </Box>
        <Box
          sx={{
            margin: "1em",
            height: "100vh",
            display: "flex",
            flexDirection: "column",
          }}
        >
          <form noValidate onSubmit={handleSubmit(onSubmit)}>
            {/* register my input into the hook by calling the register function */}
            <TextField
              {...register("email", { required: true, minLength: 5 })}
              required
              id="outlined-required"
              label="email"
              margin="normal"
              data-cy="email"
            />
            {errors.email && <span>Email is required</span>}
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              type="password"
              label="password"
              id="password"
              {...register("password", { required: true, minLength: 5 })}
              data-cy="password"
            />

            {errors.password && <span>Password is required</span>}
            <Button type="submit" variant="contained" data-cy="login">
              login
            </Button>
          </form>

          <Box>
            <p></p>
            <Divider>OR</Divider>
            <p></p>
          </Box>
          <Button variant="contained">login with google</Button>

          <Box
            sx={{
              display: "flex",
              justifyContent: "center",
              margin: "1em",
            }}
          >
            <RouterLink to="/reset">
              <Typography variant="body2">Forgot password?</Typography>
            </RouterLink>
          </Box>
          <Box
            sx={{
              display: "flex",
              justifyContent: "center",
              margin: "2em",
            }}
          >
            <Copyright version={"v 0.0.1"} />
          </Box>
        </Box>
      </Container>
    </React.Fragment>
  );
};

export default Login;

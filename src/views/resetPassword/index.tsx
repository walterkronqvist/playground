import * as React from "react";
import {
  Container,
  Box,
  Button,
  Link,
  TextField,
  Typography,
  Avatar,
} from "@mui/material";
import LockOutlinedIcon from "@mui/icons-material/LockOutlined";
import { Link as RouterLink } from "react-router-dom";
import Copyright from "../../components/Copyright";

export default function ResetPassword() {
  return (
    <React.Fragment>
      <Container maxWidth="xs">
        <Box
          sx={{
            marginTop: 8,
            marginBottom: 1.2,
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
          }}
        >
          <Avatar sx={{ m: 1, bgcolor: "primary.light" }}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography variant="h5" marginTop=".5em">
            Reset Password
          </Typography>
        </Box>
        <Box
          sx={{
            display: "flex",
            flexDirection: "column",
            marginBottom: "1.2em",
          }}
        >
          <form action="">
            <TextField
             autoFocus
              color="primary"
              required
              id="outlined-required"
              label="Type your email address"
            />
            <Button sx={{ marginTop: "1.2em" }} variant="contained" color="primary"> 
              RESET PASSWORD
            </Button>
          </form>
        </Box>
        <Box
          sx={{
            display: "flex",
            justifyContent: "center",
            margin: "1em",
          }}
        >
          <RouterLink to="/login">
            <Typography>Go to the login page</Typography>
          </RouterLink>
        </Box>
        <Box
          sx={{
            marginTop: "4em",
            display: "flex",
            justifyContent: "center",
          }}
        >
          <Copyright version={"v 0.0.1"} />
        </Box>
      </Container>
    </React.Fragment>
  );
}
